Hello, once you are done with cloning the repos please follow below steps:

- Run the below apps on Node version 10.15.3

Steps to setup Apps.
--------
Frontend
--------
1. Go to UI directory
2. Install dependencies - "npm i"
3. start the app - "npm start" 
4. app will start at - localhost:8080


--------
Backend
--------
1. Go to API directory
2. Install dependencies - "npm i"
3. start the app - "npm start" 
4. app will start at - localhost:3000

- I am using mongodb as db to store cities of US, make sure you are running mongodb locally.
- you can check for DB_CONNECTION_URL in "api/config.js" to change the connection string if needed default is mongodb://localhost:27017/poc
- The backend app is connecting to mongodb server without any auth, please make note of the same.


-- IF there are any issue with the setup you can mail me at fahad.braveknight@gmail.com