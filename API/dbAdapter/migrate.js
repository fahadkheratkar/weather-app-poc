const dump = require('./dump.json');
const { Cities } = require('../models');

module.exports = async() => {
    Cities.count().then((count) => !count && Cities.insertMany(dump))
}