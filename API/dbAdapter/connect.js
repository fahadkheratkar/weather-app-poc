const { DB_CONNECTION_URL } = require("../config");
const mongoose = require("mongoose");

// Connect to the db
const connect = new Promise(( resolve, reject) => {
    mongoose.connect(
        DB_CONNECTION_URL,
        {
          useNewUrlParser: true,
          useUnifiedTopology: true
        },
        function(err, db) {
          if (err) throw reject(err);
          resolve(true);
        }
    );
})
module.exports = { connect };