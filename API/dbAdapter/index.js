const connect = require('./connect');
const migrate = require('./migrate');

module.exports = {
    ...connect,
    migrate
}