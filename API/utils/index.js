const { WEATHER_API_KEY } = require('../config');

const generateDataUrl = ( { zipcode } ) => `https://api.openweathermap.org/data/2.5/weather?zip=${zipcode},us&&units=metric&&appid=${WEATHER_API_KEY}`

const handleHttpRes = ( result ) => result.data;

module.exports = { generateDataUrl, handleHttpRes };