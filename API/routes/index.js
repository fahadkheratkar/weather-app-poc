var express = require('express');
var router = express.Router();
const axios = require('axios');
const { Cities } = require('../models');
const { generateDataUrl, handleHttpRes } = require('../utils');

router.get('/weather/city', async (req, res) => {
	const { zipcode } = req.query;

	try {
		if(!zipcode){
			res.status(400).send({ 
				code: 0,
				data: 'Invalid Request'
			});
		}else{
			const result = await axios(generateDataUrl({ zipcode }));
			const data = handleHttpRes(result);
			res.send({ 
				code: 1,
				data, 
			});
		}
	}catch (e) {
		e.response && e.response.status === 404 ? res.status(404).send({
			code:0,
			message:'City Not Found'
		})
		:
		res.status(503).send({
			code:0,
			message:'Internal Server Error'
		})
	}
	
});

router.post('/weather/city', async (req, res) => {
	try{
		const { zipcodes } = req.body;
		if(!zipcodes){
			res.status(400).send({ 
				code: 0,
				data: 'Invalid Request'
			});
		}else{
			const results = await Promise.all(
				zipcodes.map((zipcode) => axios(generateDataUrl({ zipcode })).catch((e) => e.response ))
			);
			const data = results.map( (result) => handleHttpRes(result) );
			
			res.send({ 
				code: 1,
				data 
			});
		}
	} catch (e) {
		console.log(e)
		res.status(503).send({
			code:0,
			message:'Internal Server Error'
		})
	}
});

router.post('/cities', async ( req, res ) => {
	const { limit = 10, skip = 0, search = '' } = req.body;
	let query = {
		...( search && { city :  new RegExp(search, 'i') })
	};

	try {
		const result = await Cities.find(query)
			.skip(skip)
			.limit(limit)
			.sort({ _id : -1 });
	
		const totalDocuments = await Cities.countDocuments(query);

		res.send({ code: 1, data: { cities: result, totalDocuments} });
	} catch (e) {
		res.status(503).send({
			code:0,
			message:'Internal Server Error'
		})
	}
});

module.exports = router;
