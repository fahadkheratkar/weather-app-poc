const mongoose = require("mongoose");

var CitySchema = new mongoose.Schema({
  city: {
    type: String,
    trim: true,
    required: true
  },
  zip_code: {
    type: String,
    required: true,
  },
  latitude: {
    type: String,
  },
  longitude: {
    type: String,
  },
  state: {
    type: String,
    trim: true,
    required: true
  },
  county: {
    type: String,
    trim: true,
  },
});

var Cities = mongoose.model("cities", CitySchema);

module.exports = { Cities };