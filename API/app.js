var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const { cors } = require('./middlewares');
const { connect, migrate } = require('./dbAdapter');

/* Connect to DB */      
connect
    .then((status) => console.log("Connected to database Successfully!") ).then(()=> migrate())
    .catch((err) => console.error("err", err));

/* Routes */
var indexRouter = require('./routes/index');

/* Start App */
var app = express();

app.use(cors);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

module.exports = app;
