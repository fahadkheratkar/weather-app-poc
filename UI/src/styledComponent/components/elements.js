import styled, { css } from 'styled-components';

export const HorizontalLine = styled.hr`
	border-top: 1px solid #cacaca;
`

export const FlexBox = styled.div`
	display: ${props => props.inline ? "inline-flex" : "flex"};
	flex-direction: ${props => props.direction || "row"};
	justify-content: ${ props => props.justify || `left` };
	align-items: ${props => props.align || "stretch"};
	align-self: ${props => props.alignSelf || "auto"};
	padding: ${props => props.padding || ""};
	flex-wrap: ${props => props.wrap || "nowrap"};
	width: ${props => props.expand ? "100%" : "auto"};
`

export const Box = styled(FlexBox)`
	display: ${props => props.flex ? "flex" : "block"};
	width: ${props => ({
		tiny: "5px",
		small: "10px",
		medium: "20px",
		large: "30px",
		custom: props.customSize
	})[props.size]};
	height: ${props => ({
		tiny: "5px",
		small: "10px",
		medium: "20px",
		large: "30px",
		custom: props.customSize
	})[props.size]};
	border-radius: ${props => props.borderRadius || "0px"};
	border: ${props => props.border || "none"};
	position: ${props => props.position || "static"};
	top: ${props => props.top || ""};
	right: ${props => props.right || ""};
	bottom: ${props => props.bottom || ""};
	left: ${props => props.left || ""};
	padding: ${props => props.padding || ""};
	margin: ${props => props.margin || ""};
	text-align: ${props => props.textAlign || "left"};
`

export const ScrollBox = styled.div`
	display: block;
	overflow: auto;
	height: ${ props => props['height'] || `auto` };
	max-height: ${ props => props['maxHeight'] || `auto` };
`

export const Grid = styled.div`
	display: grid;
	grid-template-columns: ${ props => props.columns || `unset` };
	grid-template-rows: ${ props => props.rows || `unset` };
	grid-column-gap: ${props => props.columnGap || "0px"};
	grid-row-gap: ${props => props.rowGap || "0px"};
	justify-items: ${props => props.justifyItems || "start"};
	justify-content: ${props => props.justify || "start"};
	background-color: ${props => props.bgColor || "#fff"};
`

export const Block = styled.div`
	display: block;
	${ props => props.height && css`
		height : ${ props.height };
	`};

	${ props => props.width && css`
		width : ${ props.width };
	`};
`

export const RelativeBlock = styled(Block)`
	position: relative;

	${ props => props.bottom && css`
		bottom : ${ props.bottom };
	`};
`;

export const DynamicBorderBox = styled(Box)`
	border-left: ${props => props.isActive ? `3px solid ${props.borderColor}` : "3px solid transparent"};
    margin: 7px 10px;
	background: ${props => props.isActive ? "fff" : "#F5F4F4"};
    padding: 15px 20px;
    box-shadow: 0px 0px 3px 1px #c2c2c2;
`

export const ColorIndicator = styled.div`
	width: 5px;
	height: 5px;
	background-color: ${props => props.bgColor || "#fff"};
`