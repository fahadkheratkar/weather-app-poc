import styled from 'styled-components';

export const Table = styled.table`
    margin: ${ props => props.margin || '.5rem 0px' };
    width: ${ props => props.width || '100%' };
    border-collapse: separate;
    border-spacing: 0;
`
export const TableHead = styled.thead`
    background: whitesmoke;
`
export const TableRow = styled.tr`
    transition: all 0.3s;

    &:hover{
        ${props => props.highlightOnHover ? `background-color: ${props.highlightOnHover}` : "" };
    }

    tbody &{
        background-image: linear-gradient(to right, #dadada 70%, rgba(255, 255, 255, 0) 20%);
        background-position: bottom;
        background-size: 13px 1px;
        background-repeat: repeat-x;
    }
`

export const TableCell = styled.td`
    ${TableHead} &{
        padding: 5px 15px;
        color: #43424b;
        /* font-family: HelveticaNeueLTPro-Md; */
        font-weight: 500;
    }

    text-align: ${ props => props['text-align'] || 'left' };
    width: ${ props => props.width || 'auto'};
    min-width: 20px;
    padding: 15px 15px;
    font-size: 1em;
    font-weight: 400;
    color: ${ props => props['color'] || '#43424b' };
`
