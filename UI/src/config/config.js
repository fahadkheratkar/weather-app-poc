const DEV = {
    host: "http://localhost:3000",
};

const PROD = {
  host: "http://localhost:3000",
};

module.exports =
  process.env.REACT_APP_ENV === "PROD"
    ? PROD
    : DEV;
