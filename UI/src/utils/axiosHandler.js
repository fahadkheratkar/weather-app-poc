import { toast } from "react-toastify";
import axios from "axios";
import { host } from "../config/config";

const validateStatus = status => {
  return status >= 200 && status <= 400;
};

export const post = (path, data, options = {}) => {
  return new Promise((resolve, reject) => {
    axios
      .post(host + path, data, {
        headers: {
          "Content-Type": "application/json",
          ...options.headers
        },
        validateStatus: validateStatus,
        ...options
      })
      .then(res => {
        if (options.successMessage && res.data.code) {
          toast.success(options.successMessage);
        }
        resolve(res.data);
      })
      .catch(e => {
        e.response && toast.error(
          options.errorMessage ? options.errorMessage : `Error Occurred ${e}`
        );
        reject(e);
      });
  })
};

export const get = (path, data, options = {}) => {
  return new Promise((resolve, reject) => {
    axios
      .get(host + path, {
        params: data,
        headers: {
          "Content-Type": "application/json",
          ...options.headers
        },
        validateStatus: validateStatus,
        ...options,
      })
      .then(res => {
        if (options.successMessage && res.data.code) {
          toast.success(options.successMessage);
        }
        resolve(res.data);
      })
      .catch(e => {
        e.response && toast.error(
          options.errorMessage ? options.errorMessage : `Error Occurred ${e}`
        );
        reject(e);
      });
  })
};

export const put = (path, data, options = {}) => {
  return new Promise((resolve, reject) => {
    axios
      .put(host + path, data, {
        headers: {
          "Content-Type": "application/json",
          ...options.headers
        },
        validateStatus: validateStatus,
        ...options
      })
      .then(res => {
        if (options.successMessage && res.data.code) {
          toast.success(options.successMessage);
        }
        resolve(res.data);
      })
      .catch(e => {
        e.response && toast.error(
          options.errorMessage ? options.errorMessage : `Error Occurred ${e}`
        );
        reject(e);
      });
  })
};

export const patch = (path, data, options = {}) => {
  return new Promise((resolve, reject) => {
    axios.patch(host + path, data, {
      headers: {
        "Content-Type": "application/json",
        ...options.headers
      },
      validateStatus: validateStatus,
      ...options
    })
    .then(res => {
      if (options.successMessage && res.data.code) {
        toast.success(options.successMessage);
      }
      resolve(res.data);
    })
    .catch(e => {
      e.response && toast.error(
        options.errorMessage ? options.errorMessage : `Error Occurred ${e}`
      );
      reject(e);
    });
  })
};