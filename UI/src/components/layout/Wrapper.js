import React, { Component } from "react";
import CitiesList from "../cities/CityList.jsx";
import WeatherDetail from "../cities/WeatherDetail.jsx";

class Wrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return (
      <div id="wrapper">
        <div id="page-content-wrapper">
          <CitiesList />
          <WeatherDetail />
        </div>
      </div>
    );
  }
}

export default Wrapper;