import React, { Component } from 'react'
import LoadingOverlay from "react-loading-overlay";
import Popup from "reactjs-popup";
import { connect } from "react-redux";
import { CloseButton } from '../../styledComponent/components/button'
import { FlexBox } from '../../styledComponent/components/elements'
import { setModalStatus } from '../../redux/actions'

class WeatherDetail extends Component{

	closeModal = () => this.props.setModalStatus(false)

	render() {
		const { 
			isModalOpen,
			isDataLoading,
			weatherDetails : {
				main : {
					temp = 0,
            		feels_like = 0,
            		temp_min = 0,
            		temp_max = 0,
            		pressure = 0,
            		humidity = 0
				} = {},
				name = '',
				sys : { country = '' } = {},
				coord : {
					lon = 0,
					lat = 0
				} = {},
				wind : {
					speed = 0,
				} = {},
				clouds : {
					all = 0
				} = {},
			} = {}
		} = this.props;

		return (
			<Popup 
				open={isModalOpen} 
				onClose={() => this.closeModal()}
				className="modal" 
			>
				<div className="popupContent">
					<LoadingOverlay
						active={isDataLoading}
						spinner
					>
						<div className={ "modalView" }>
							<FlexBox className="modalHead py-2">
								<h4 className="m-auto">Today's Weather</h4>
								<CloseButton onClick={this.closeModal} />
							</FlexBox>
							<div className="modalBody p-3">
								<h5 className="mb-3 text-center"><span className="badge badge-secondary">{temp}° С</span> - {name}, {country}</h5>
								<table className="table table-hover">
									<tbody>
										<tr>
											<td>Temperature</td>
											<td>{temp_min} to {temp_max} °С</td>
										</tr>
										<tr>
											<td>Feels Like</td>
											<td>{feels_like} °С</td>
										</tr>

										<tr>
											<td>Humidity</td>
											<td>{humidity} %</td>
										</tr>
										<tr>
											<td>Wind</td>
											<td>{speed} m/s</td>
										</tr>
										<tr>
											<td>Clouds</td>
											<td>{all} %</td>
										</tr>
										<tr>
											<td>Pressure</td>
											<td>{pressure} hpa</td>
										</tr>

										<tr>
											<td>Geo coords</td>
											<td>[{lon}, {lat}]</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</LoadingOverlay>
				</div>
			</Popup>
		);
	}
}

const mapStateToProps = state => {
    return {
		weatherDetails : state.weatherDetails.data,
		isDataLoading : state.weatherDetails.isDataLoading,
		isModalOpen : state.weatherDetails.isModalOpen,
    };
};
  
export default connect(
    mapStateToProps,
    {
		setModalStatus    
    }
)(WeatherDetail);