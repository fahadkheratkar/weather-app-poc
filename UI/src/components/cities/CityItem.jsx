import React from "react";
import { connect } from "react-redux";
import { TextualButton } from "../../styledComponent/components/button";
import { TableRow, TableCell } from "../../styledComponent/components/table";
import { getWeatherInfo } from "../../redux/actions";


const CityItem = props => {
    const { city, index } = props;

    const handleItemClick = async () => {
        props.getWeatherInfo({ zipcode : props.city.zip_code })
    }

    return (
        <TableRow highlightOnHover="hsl(0, 0%, 97%)">
            <TableCell text-align="center">{index}</TableCell>
            <TableCell text-align="center"><span style={{textTransform:'capitalize'}}>{city.city}</span></TableCell>
            <TableCell text-align="center">{city.latitude || '-'}</TableCell>
            <TableCell text-align="center">
                {city.longitude || '-'}
            </TableCell>
            <TableCell text-align="center">
                <span style={{textTransform:'capitalize'}}>{city.state}</span>
            </TableCell>
            <TableCell text-align="center">
                <span style={{textTransform:'capitalize'}}>{city.county}</span>
            </TableCell>
            <TableCell text-align="center" onClick={handleItemClick}>
                <TextualButton>{`Check Weather`}</TextualButton>
            </TableCell>
        </TableRow>
    );   
};

const mapStateToProps = state => {
    return {
        cities : state.cities.data
    };
};
  
export default connect(
    mapStateToProps,
    {
        getWeatherInfo
    }
)(CityItem);