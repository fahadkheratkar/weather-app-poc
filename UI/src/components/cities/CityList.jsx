import React, { Component } from "react";
import { connect } from "react-redux";
import ReactPaginate from "react-paginate";
import { Table, TableRow, TableCell, TableHead } from "../../styledComponent/components/table";
import { Block } from "../../styledComponent/components/elements";
import CityItem from './CityItem';
import { 
  getCities,
  setPaginationPage
} from "../../redux/actions";

const listRowsPerPage = 10;

class CityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }

  }

  componentDidMount(){
    this.props.getCities();
  }

  handlePageClick = ({selected}) => {
    this.props.setPaginationPage(selected)
    this.props.getCities({ skip : selected * listRowsPerPage})
  }

  render() {
    const pageCount = Math.ceil((this.props.totalCities-1)/listRowsPerPage)

    return (
      <Block className="bg-white">
        <div className="module p-3" style={{ backgroundColor: "#fff" }}>
          <>
            <div className="row mt-2 mb-2">
              <div className="col-md-12">
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell key={`#`} text-align="center" >#</TableCell>
                  <TableCell key={`city`} text-align="center">City</TableCell>
                  <TableCell key={`lat`} text-align="center">Latitude</TableCell>
                  <TableCell key={`lon`} text-align="center">Longitude</TableCell>
                  <TableCell key={`state`} text-align="center">State</TableCell>
                  <TableCell key={`county`} text-align="center" >County</TableCell>
                  <TableCell key={`status`} text-align="center" >Action</TableCell>
                </TableRow>
              </TableHead>
              <tbody>
                { this.props.cities.length > 0 &&
                  this.props.cities.map((city, index) => <>
                    <CityItem
                      key={city._id}
                      city={city}
                      index={ (this.props.paginationPage * 10) + (index + 1) } 
                    />
                  </> 
                  )
                }
              </tbody>
            </Table>
            {
              this.props.cities.length > 0 &&
              <ReactPaginate
                previousLabel={pageCount > 1 ? "< Back": null}
                nextLabel={pageCount > 1 ? "Next >" : null}
                breakLabel={"..."}
                pageCount={pageCount}
                marginPagesDisplayed={3}
                pageRangeDisplayed={2}
                onPageChange={this.handlePageClick}
                containerClassName={"pagination notSelectableText"}
                activeClassName={"paginationActivePage"}
                forcePage={this.props.paginationPage}
              />
            }
            </div>
          </div>
          </>
        </div>
    </Block>
    );
  }
}

const mapStateToProps = state => {
  return {
     cities : state.cities.data,
     totalCities : state.cities.totalDocuments,
     paginationPage: state.cities.paginationPage
  };
};

export default connect(
  mapStateToProps,
  {
    getCities,
    setPaginationPage
  }
)(CityList);
