import React, { Component } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import Wrapper from "./components/layout/Wrapper.js";
import { store } from "./redux/index";
import { Provider } from "react-redux";
import { ThemeProvider } from "styled-components";
import Default from "./styledComponent/themes";
import "./css/index.scss";


toast.configure({
  position: "bottom-right",
  autoClose: 5000,
  closeOnClick: true,
  rtl: false,
  draggable: false,
  pauseOnHover: true
});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ThemeProvider theme={Default}>
        <div className="App">
          <Wrapper />
        </div>
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
