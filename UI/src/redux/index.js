import { allReducers } from "./reducers/index.js";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

let composeArgs = [applyMiddleware(thunk)];
// process.env.REACT_APP_ENV !== "PROD" && process.env.REACT_APP_ENV !== "TEST" && composeArgs.push(window.devToolsExtension && window.devToolsExtension());

const storeEnhancers = compose(...composeArgs);

export const store = createStore(allReducers, {}, storeEnhancers);
