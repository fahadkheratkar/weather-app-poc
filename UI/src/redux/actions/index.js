export { 
    getCities,
    setPaginationPage,
    updateCities
} from './cities'

export {
    setIsLoadingStatus,
    setModalStatus,
    getWeatherInfo
} from './weatherDetails'
