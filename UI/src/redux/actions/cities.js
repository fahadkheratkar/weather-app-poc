import { post } from "../../utils/axiosHandler";

export const getCities = data => async ( dispatch, getState) => {
	
  	const result = await post("/cities", {
		...data,
	});

	if(result.code && result.data ){
		dispatch(getCitiesHelper({ ...result.data }));
	  	return result;
	}
};

export const getCitiesHelper = data => ({
	type: "SET_CITIES",
	payload: data
});


export const setPaginationPage = data => dispatch => dispatch({
	type: "SET_CITIES_PAGE",
	payload: data
});

export const updateCities = data => dispatch => dispatch(updateCitiesHelper(data));

export const updateCitiesHelper = data => ({
	type: "UPDATE_CITIES",
	payload: data
});
