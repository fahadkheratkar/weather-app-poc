import { get } from "../../utils/axiosHandler";

export const getWeatherInfo = data => async ( dispatch, getState) => {
    dispatch(setModalStatusHelper(true));
    dispatch(setIsLoadingStatus(true));
	
  	const result = await get("/weather/city", {
		...data,
	}).catch((e) => dispatch(setIsLoadingStatus(false)));

    dispatch(setIsLoadingStatus(false));

	if(result.code && result.data ){
        dispatch(getWeatherInfoHelper({ data : result.data }));
	  	return result;
	}
};

export const getWeatherInfoHelper = data => ({
	type: "SET_WEATHER_DATA",
	payload: data
});


export const setModalStatus = data => dispatch => dispatch(setModalStatusHelper(data));

export const setModalStatusHelper = data => ({
	type: "SET_MODAL_STATUS",
	payload: data
});

export const setIsLoadingStatus = data => dispatch => dispatch(setIsLoadingStatusHelper(data));

export const setIsLoadingStatusHelper = data => ({
	type: "SET_LOADING_STATUS",
	payload: data
});
