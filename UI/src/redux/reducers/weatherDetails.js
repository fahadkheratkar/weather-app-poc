
const initialState = {
    data: {},
    isDataLoading: false,
    isModalOpen: false,
};

export default function(state = initialState, action) {
    switch (action.type) {
        case "SET_WEATHER_DATA":
        return {
            ...state,
            data: action.payload.data,
        };
        case "SET_MODAL_STATUS":
        return {
            ...state,
            isModalOpen: action.payload,
        };
        case "SET_LOADING_STATUS":
        return {
            ...state,
            isDataLoading: action.payload,
        }
        default:
            return state;
    }
};