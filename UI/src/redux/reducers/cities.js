
const initialState = {
    data: [],
    totalDocuments: 0,
    paginationPage: 0,
};

export default function(state = initialState, action) {
    switch (action.type) {
        case "SET_CITIES":
        return {
            ...state,
            data: [ ...action.payload.cities ],
            totalDocuments: action.payload.totalDocuments,
        };
        case "UPDATE_CITIES":
        return {
            ...state,
            data: [ ...action.payload.cities ],
        };
        case "SET_CITIES_PAGE":
        return {
            ...state,
            paginationPage: action.payload,
        }
        default:
            return state;
    }
};