import { combineReducers } from "redux";
import cities from "./cities";
import weatherDetails from "./weatherDetails";


export const allReducers = combineReducers({
  cities,
  weatherDetails
});
